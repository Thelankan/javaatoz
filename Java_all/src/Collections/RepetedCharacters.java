package Collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class RepetedCharacters {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		String s="LankakkLLLLL";
		char[] str=s.toCharArray();
		
//########### Approach one ################ 
		ArrayList al=new ArrayList();
		
		for (int i=0;i<str.length;i++){
			
			int k=0;
			
			if (!al.contains(str[i])){
				al.add(str[i]);
				k++;
				
				for (int j=i+1;j<str.length;j++){
					
					if (str[i]==str[j]){
						k++;
					}
					
				}
				System.out.println(str[i]+" is printing "+k+" Times ");
			}
			
		}
		
//################# Approach two #####################
		
/*	Set rep=new HashSet();
	int count=0;
		
	for (int z=0;z<str.length;z++){
		
		rep.add(str[z]);
		
		for (int x=0;x<str.length;x++){
			if(str[z]==str[x]){
			count++;
			}
			
			System.out.println(str[z]+" is printing "+x+" Times ");
			
		}
		
		
	}*/
	
		
		int[] arry={1,2,3,4};
	
		int i=0;
		
		while(i<arry.length-1){
			System.out.println(arry[i]);
			i--;
		}
		
		int low=0;
		int high=arry.length;
		int temp;
		
		   while(low<high){
               temp= arry[low];
               arry[low] = arry[high];

               System.out.print(arry[low]);


               low ++;
               high--;
           }
		

	}

}

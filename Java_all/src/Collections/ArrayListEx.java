package Collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

public class ArrayListEx extends Object{
	
	

	public static void main(String[] args) {
		
		//Array List
		ArrayList a=new ArrayList();
		a.add("1");
		a.add("3");
		a.add("4");
		a.add("6");
		a.add(" ");
		a.add("8");
		a.add(8);
		for(Object o:a) {
			System.out.println(o);
		}
		
		//Linked List
		LinkedList l=new LinkedList();
		l.add(1);
		l.add(3);
		l.add(4);
		l.add(" ");
		l.add(8);
		l.add("srv");
		l.add(null);
		for(Object o:l) {
			System.out.println(o);
		}
		
			//Priority Queue
		PriorityQueue p=new PriorityQueue();
		p.add(1);
		p.add(6);
		p.add(9);
		p.add(3);
		p.add(4);
		
		
		//Stores same type data
		//insertion order cannot be preserved
		//get method is not avai;lable to read data, we have to read using peek(),poll() or using for each loop
		
		for(Object o:p) {
			System.out.println(o);
		}
		
	System.out.println(p.peek());
	p.poll();
	p.poll();
	for(Object o:p) {
		System.out.println(o);
	}
		
	/*	//Hash set
		HashSet h=new HashSet();
		h.add(1);
		h.add(4);
		h.add("3");
		h.add(6);
		h.add(9);
		
		//wont preserve insertion order
		//we can store different type data
		
		for(Object o:h) {
			System.out.println(o);
		}
		
		//@@@@@@@@@@@@@@@@@@@ tree set @@@@@@@@@@@@
		TreeSet t=new TreeSet();
		t.add(1);
		t.add(4);
		t.add(3);
		t.add(6);
		t.add(9);
		t.add(9);
		
		//can store only similar type data
		//insertion order it wont preserve it will return based on sorted order and null and duplicate values is not allowed
		//get method not available to read data, we can read data only using for -each loop
		
		for(Object o:t) {
			System.out.println(o);
		}
		Iterator i=t.iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
		}
		
		//@@@@@@@@@@@@@@ hash Map @@@@@@@@@@@@@@@@@@@
		HashMap<String,Object> hm=new HashMap<String,Object>();
		hm.put("Lanka", "PFS Emploee");
		hm.put("From", "Hindupur");
		hm.put("Age", 27);
		
		//System.out.println(hm.get("From"));

		//########### Keyset practice ################
		//works based on key and return
		Set<String> s=hm.keySet();
		
		for(String s1:s) {
			System.out.println(hm.get(s1));
		}
	}
	*/
	
//Comparing array list
	
/*	public class ListCompare {

	    public static boolean compareList(List ls1,List ls2){
	        return ls1.toString().contentEquals(ls2.toString())?true:false;
	    }
	    public static void main(String[] args) {

	        ArrayList<String> one  = new ArrayList<String>();
	        ArrayList<String> two  = new ArrayList<String>();

	        one.add("one");
	        one.add("two");
	        one.add("six");

	        two.add("one");
	        two.add("two");
	        two.add("six");

	        System.out.println("Output1 :: "+compareList(one,two));

	        two.add("ten");

	        System.out.println("Output2 :: "+compareList(one,two));
	    }*/
	}	

}

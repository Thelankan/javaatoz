package Collections;

import java.util.ArrayList;

public class StringSentanceReverse {
	
	public static void main(String[] args) {
		
		String str="any string can be reversable here.amit#vinay*Antoni1com";
		char[] characters;
		
		//str.replaceAll("[^a-zA_Z]", " ")
		int len=str.split(" ").length;
		
		System.out.println(len);
		
		// ########## Using Normal way #############
		ArrayList<String> al=new ArrayList();
		for (int i=0;i<len;i++){	
		al.add(str.split(" ")[i]);
		characters=al.get(i).toCharArray();
			
		for(int z=characters.length-1;z>=0;z--){
			System.out.print(characters[z]);	
		}
		System.out.print(" ");
		
		}
		
		
/*	//############## Using String Builder ##############	
	ArrayList<String> al=new ArrayList();
		
		for (int i=0;i<len;i++){	
		al.add(str.split(" ")[i]);
		//characters=al.get(i).toCharArray();
		}
		
		int size = al.size();
		for(int y = 0; y < size; y++)
		{
		    StringBuilder builder = new StringBuilder(al.get(y));
		    builder.reverse();
		    al.set(y, builder.toString());
		}
		
		System.out.println(al);*/
	
		
		
  }
		
}

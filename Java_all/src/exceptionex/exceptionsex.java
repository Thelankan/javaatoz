package exceptionex;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class exceptionsex {

	public static void main(String[] args) throws FileNotFoundException {
		
		int a=10;
		int b=0;
		String s="aa";
		String s1="sunil";
		int i[]=new int[]{1,2,3,4};
		
		
		try {
			System.out.println(a/b);
		} catch (ArithmeticException ee) {
			System.out.println(ee.getMessage());
			System.out.println("ArithmeticException");
		}
		
		try {
			System.out.println(s.charAt(0));
		} catch (NullPointerException fe) {
			System.out.println(fe.getMessage());
			System.out.println("Null pointer");
		}
		
		try {
			System.out.println(s1.charAt(20));
		} catch (StringIndexOutOfBoundsException se) {
			System.out.println(se.getMessage());
			System.out.println("StringOutOfBoundsException");
		}
		
	try {
		System.out.println(i[8]);
	} catch (ArrayIndexOutOfBoundsException ae) {
		System.out.println(ae.getMessage());
		System.out.println("ArrayOutOfBoundsException");
	}

		
		System.out.println("before try catch");
		
		try {
			FileReader fr=new FileReader("something.txt");
		} catch (FileNotFoundException fnf) {
			System.out.println(fnf.getMessage());
			System.out.println("file not found");
		}

		System.out.println("after try catch");
		
		System.out.println("Before execution");
		FileReader fr=new FileReader("something.txt");
		System.out.println("After execution");	
		
		
	
		try {
			System.out.println(a/b);
			System.out.println(s.charAt(0));
			System.out.println(s1.charAt(20));
			System.out.println(i[8]);
		}
		catch (ArithmeticException ae) { System.out.println("ArithmeticException"); }
		catch (NullPointerException ne) { System.out.println("Null pointer"); }
		catch (StringIndexOutOfBoundsException se){ System.out.println("StringOutOfBoundsException"); }
		catch (ArrayIndexOutOfBoundsException aae){System.out.println("ArrayOutOfBoundsException"); }
		catch (Exception e){}
		
		try {
			System.out.println(a/b);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		finally{
			System.out.println("Always executes");
		}
		
		
		

	}

}

package com.inheritance;

class C{
	
	void m1(){
		System.out.println("Class C method");
	}
}

class D extends C{
	void m2(){
		System.out.println("Class D method");
	}
	
}


class E extends C{
	
	void m3(){
		System.out.println("Class E method");
	}
}



public class SingleLevelInheritence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

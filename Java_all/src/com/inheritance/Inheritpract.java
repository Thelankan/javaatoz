package com.inheritance;


class a{
	
	protected int i=0;
	
	private String s="private inherit"; //private members cannot be inherited 
	
	protected static int z=10;
	
	protected void method()
	{
		System.out.println("method");
	}
	
	protected a(){
		System.out.println("construtor");
	}
}

class b extends a{
	
	
	protected b(){
		super();
		System.out.println("construtor in calss b");
	}
	
}

public class Inheritpract {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		a aclass=new b();
		
		aclass.method();
		
		System.out.println(aclass.i);
		System.out.println(aclass.z);
		//System.out.println(aclass.s); //private members cannot be inherited 
		
		
		

	}

}

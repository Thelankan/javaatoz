package com.inheritance;

class F{

 F()
 {
	 System.out.println("Class F constructor");
 }
	void m1(){
		System.out.println("Class C method");
	}
}

class G extends F{
	
	G()
	 {
		 System.out.println("Class F constructor");
	 }
	void m2(){
		System.out.println("Class D method");
	}
	
}


class H extends G{
	
	H()
	 {
		 System.out.println("Class F constructor");
	 }
	
	void m3(){
		System.out.println("Class E method");
	}
}



public class MultilevelInheritence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 new H();
 
	}

}

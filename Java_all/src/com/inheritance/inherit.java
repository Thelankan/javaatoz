package com.inheritance;

public class inherit {
	
	//Static private and constructors will not participate in inheritance
	//Final also not participate in inheritance
	
	//visibility can not reduce while overriding
	
	//private
	//default
	//protected
	//public
	
	//non static we cannot use inside static 

	
	//Method overloading only one we can create with one method type 
	//it will call only one method if u call by parameter
	
	//private constructor we can not create object
	
	//local variable inside methods can not be categorized as static and non static

}

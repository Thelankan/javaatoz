package com.inheritance;


class Human{
	
	 void eat()
	{
		System.out.println("Human");
	}
}
class Boy extends Human{
	
	public void eat()
	{
		System.out.println("Boy");
	}
}


public class MethodOverRidding {

	public static void main(String[] args) {
	
		// TODO Auto-generated method stub

	Boy boy=	new Boy();
	
	boy.eat();
	}

}

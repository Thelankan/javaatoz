package com.practice;

class bachelor{
	
	void excursion(){
		System.out.println("going to excurtion with friends");
	}
	
	bachelor(){
		System.out.println("bachelor constructor");
	}
}

class NewlyMarried extends bachelor{
	
	void excursion(){
		System.out.println("going to excurtion with wife");
	}
	
	void method2(){
		System.out.println("new method in newly married");
	}
	
	NewlyMarried(){
		super();
		System.out.println("Newly married constructor");
	}
	
}

class Aftermarriage extends NewlyMarried{
	
	void excursion(){
		System.out.println("no excuurtions");
	}
	
}

public class poly {

	public static void main(String[] args) {
		
//		bachelor b=new bachelor();
//		b.excursion();
//		
//		bachelor c=new NewlyMarried();//upcasting
//		c.excursion();
//		
//		NewlyMarried n=(NewlyMarried) c; //downcast already upcasted one  is possible
//		n.excursion();
//		
//		NewlyMarried t=(NewlyMarried) new bachelor(); //direct downcast is not possible
//		t.excursion();
		
//		
//		Aftermarriage af=new Aftermarriage();
//		af.excursion();
//		
//		NewlyMarried nw=new Aftermarriage();
//		nw.excursion();
		
//		bachelor c=new NewlyMarried();//upcasting
//		c.method2();
		
		new NewlyMarried();
		
		
}
}


//class compile{
//	
//void a(int i){
//	System.out.println("i");
//	
//}
//	
//void b(int f,int j){
//	System.out.println("j");
//		
//	}
//
//void c(double d){
//	System.out.println("k");
//}
//	
//}

package com.practice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;

class games implements Serializable{
	
	int id;
	String s;
	
	public games(int id,String s){
		this.id=id;
		this.s=s;
		
	}
	
}

public class serializeex {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		games g1=new games(100,"vinay Lanka");
		
		//############# serialization ################
		FileOutputStream fos=new FileOutputStream("File.txt");
		ObjectOutputStream objo=new ObjectOutputStream(fos);
		
		objo.writeObject(g1);
		objo.flush();
		System.out.println("success");
		
		//############# de serialization ################
		ObjectInputStream obinput=new ObjectInputStream(new FileInputStream("File.txt"));
		games g=(games)obinput.readObject(); 
		System.out.println(g.id+" "+g.s);
		
	}
	
}


package com.practiceall;

public class exe{
	
	public static void main(String[] args) {
		
		exe1 a=new exe1();
		a.type();
		
		//Wildanimals w=(Wildanimals) new exe1(); //down casting
		
		exe1 a1=new Wildanimals(); //up casting
		
			Wildanimals wa=(Wildanimals) a1; //down casting
			wa.behavoir();
			
			Wildanimals g=(Wildanimals) new exe1();//Run time
			
	}
}
 
class exe1 {
	
	void type(){
		System.out.println("wild,domestic,reptails");
	}
	
}

class Wildanimals extends exe1{
	
	void type(){
		System.out.println("only wild animals");
	}
	
	void behavoir(){
		System.out.println("very wild");
	}
	
	void extras(){
		System.out.println("wild and kill the humans");
	}
	
}

class Reptails extends exe1{
	
	void type(){
		System.out.println("only wild animals");
	}
	
	void behavoir(){
		System.out.println("poisonous");
	}
	
	void extras1(){
		System.out.println("poisonous Reptails");
	}
	
}





package com.updatedpract;

public class Class1 {

	
	//instance variable (Defines property of object) .. inside the class and not inside the method
	String name;
	int age;
	
	//Static variable (belong to a class) class variable
	static int count;
	
	public void method(int a){
		
		//Local Variable
		int i=a;
		
	}
	
	public static void main(String[] args) {
		
	
	}

}

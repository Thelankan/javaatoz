package com.updatedpract;


interface calculate{
	
	public double area(float side);
	public double perimeter(float side);
	
}

class Square implements calculate {
	
	
	double area;
	double perimeter;

	@Override
	public double area(float side) {
		
		area=side*side;
		//System.out.println(area);
		return area;
		
		
	}

	@Override
	public double perimeter(float side) {
		
		perimeter=4*side;
		//System.out.println(perimeter);
		return perimeter;
		
	}
	
	public static void main(String[] args){
		
		calculate c1=new Square();
		System.out.println(c1.area(4));
		System.out.println(c1.perimeter(5));

		
	}


}

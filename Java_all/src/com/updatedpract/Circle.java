package com.updatedpract;


interface shape{
	
	public void area(double pie,float r);
	public void circumference(double pie,double r);
	
	
}

class Circle implements shape {
	
	public void area(double pie,float r) {
		
	double area;
	area=pie*r*r;
	System.out.println("Area:-"+area);
	
	}

	public void circumference(double pie,double r) {
		
		double circumference;
		circumference=2*pie*r;
		System.out.println("circumference:-"+circumference);
		
		
	}
	
	public static void main(String[] args){
		
		shape s1=new Circle();
		s1.area(3.14,3);
		s1.circumference(3.14,2);
		
	}


}

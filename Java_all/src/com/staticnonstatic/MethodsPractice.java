package com.staticnonstatic;

public class MethodsPractice {
	int i;
	
	 static int m1()
	{
		int i = 1;
		System.out.println(" m1 "+i);
		m2();
		return i;	
		//m2();
	}
	
	public static void m2()
	{
		char j = 'd';
		System.out.println(" m2 "+j);
	}
	
//	public  void m3()
//	{
//		char j = 'd';
//		System.out.println(" m2 "+j);
//		m1();
//		m2();
//	}
	
	public static void main(String agrs[])
	   {
	      m1();
	      
	      m2();
		
	   }

}

// methods syntax
// public,private,protected methods
//void method and return types
//unreachable statements
//methods uses STACK flow - LIFO
//Access specifiers :public,private,default,protected
//modifiers : static and non-static
//return types concept
//arguments
//calling methods
//cannot access non-static methods in static methods
package com.staticnonstatic;

public class MethodsOverloading {

	
	private static int m1(int x)
	{
		int i = 1;
		System.out.println(" m1 "+i);
		
		return i;	
		
	}
	
	public static void m1()
	{
		char j = 'd';
		System.out.println(" m2 "+j);
	}
	public static float m1(float m)
	{
		char j = 'd';
		System.out.println(" m2 "+j);
		return m;
	}
	
	public static void m1(char x)
	{
		char j = 'd';
		System.out.println(" m2 "+j);
		
		
	}
//	
//	public static void m2(int x , int y)
//	{
//		char j = 'd';
//		System.out.println(" m2 "+j);
//	}
//	
//	public static void m2(int a , int b)
//	{
//		char j = 'q';
//		System.out.println(" m2 "+j);
//	}
	
	public static void m2(int x , char y)
	{
		char j = 'q';
		System.out.println(" m2 "+j);
	}
	
	public static void m2(char x, int y)
	{
		char j = 'q';
		System.out.println(" m2 "+j);
	}
	
	public static void main(String agrs[])
	   {
	      m1();
	      m1(2);
	      m1('s');
		
	   }

}

//methods over loading
//method over loading depends only on method name and arguments. Not depend on return type
//in side method local variables inililization mandatory: like int i = 0;

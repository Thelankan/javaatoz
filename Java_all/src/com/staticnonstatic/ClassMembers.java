package com.staticnonstatic;

public class ClassMembers {
	
	public static int i; //data member if not intialized it intialze to default values.
	public  int x;
	
	public static void m1() // member function
	{
		char j = 'd'; //local variable: initization mandotory
		System.out.println(" m2 "+j);
	}
	
	public void m2() // member function
	{
		char j = 'd'; //local variable: initization mandotory
		System.out.println(" m2 "+j);
	}
	
	public static void main(String agrs[])
	   {
	    
		System.out.println(i); //we can access static data member without reference
		
//		System.out.println(x); // non-static members(datan function) cannot able to access in static methos.
		
	   }

}


///Multiple classes
class A{

	void m1(){
		
		int i = 0;
		System.out.println(i);
	}
	
}

//public class B{
//
//	void m1(){
//		
//		int i = 0;
//		System.out.println(i);
//	}
//	
//}


class C{

	void m1(){
		
		int i = 0;
		System.out.println(i);
	}
	
}

//If file contain one public class we should need to give file name as that class name
//If there is no public classes we can give any name or any one's class name for file



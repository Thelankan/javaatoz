package com.sample;

import java.util.Iterator;
import java.util.LinkedList;

public class Sample5 {

	public static void main(String[] args) {
		 LinkedList ll=new LinkedList();
		 
		 ll.add("Books");
		 ll.add("PFS");
		 ll.add("400");
		 
		 Iterator itr=ll.iterator();
		 
		 while(itr.hasNext())
		 {
			 System.out.print(" "+itr.next());
		 }
		 System.out.println("********");

		 ll.addLast("LiveArea");
		 Iterator itr1=ll.iterator();
		 
		 while(itr1.hasNext())
		 {
			 System.out.print(" "+itr1.next());
		 }
		 System.out.println("********");

		 ll.set(1, "Temp");
		 Iterator itr2=ll.iterator();
		 
		 while(itr2.hasNext())
		 {
			 System.out.print(" "+itr2.next());
		 }
		 
		 
		 ll.clear();
		 System.out.println("********");


		 Iterator itr4=ll.iterator();
		 
		 while(itr4.hasNext())
		 {
			 System.out.print(" "+itr4.next());
		 }
		 
		 
	}

}

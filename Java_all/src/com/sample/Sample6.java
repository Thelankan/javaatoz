package com.sample;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;



public class Sample6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Set<String> st=new HashSet<String>();
		LinkedHashSet st= new LinkedHashSet();
		
		//System.out.println(st);
		
		st.add("2");
		st.add("4");
		st.add("2");
		st.add("5");
		st.add("NULL");
		System.out.println("size of set is "+st.size());
		
		System.out.println(st);
		
		System.out.println("Using iterator");
		Iterator itr=st.iterator();
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	
		ArrayList at=new ArrayList();
		
		at.add("5");
		at.add("6");

		st.addAll(at);
		System.out.println("Using addall");
		for(Object a:st)
		{
			System.out.println(a);
		}
		
		
		ArrayList at1=new ArrayList();
		
		at1.add("7");
		at1.add("8");
		at1.add("NULL");
		at1.add("6");
		System.out.println("Using retainall");
		st.retainAll(at1);
		for(Object a:st)
		{
			System.out.println(a);
		}
		
		st.removeAll(at);
		System.out.println("Using removeall");
		for(Object a:st)
		{
			System.out.println(a);
		}

	}

}

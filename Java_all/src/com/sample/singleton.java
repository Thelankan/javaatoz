package com.sample;

class SG
{
 //private
	private static SG sg;
	int i;
	
	//private contructor
	private SG()
	{
		System.out.println("Private constructor");
	}
	
	//public method
	public static synchronized SG getInstance()
	{
		System.out.println("Get instance");
		if(sg==null)
		{
			sg=new SG();			
		}
		return sg;
	}
}


public class singleton {

	public static void main(String[] args) {
		SG sg1=SG.getInstance();
		
		sg1.i=20;
		System.out.println("First time"+sg1.i);
		
		sg1.i=30;
		System.out.println("Second time"+sg1.i);
		
		
	}

}

package com.sample;

import java.util.ArrayList;
import java.util.List;

public class Sample3 {

	public static void main(String[] args) {
		
		List list=new ArrayList();
		list.add("Object");
		list.add("Class");
		list.add("Interface");
		
		for(int i=0;i<list.size();i++)
		{
			System.out.print(list.get(i)+" ");
		}
		
		System.out.println("*******");
		list.remove("Class");
		
		for(int i=0;i<list.size();i++)
		{
			System.out.print(list.get(i)+ " ");
		}
	}

}

package com.sample;
class En
{
	//private variable 
	private String UserName;
	private String Password;
	//public getter & setter
	//getter
	public String getUserName()
	{
		return UserName;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setUserName(String Un)
	{
		UserName=Un;
	}
	public void setPassword(String pwd)
	{
		Password=pwd;
	}
}
public class Encapsulation {
	public static void main(String[] args) {
		En en=new En();
		en.setUserName("PFS");
		en.setPassword("LiveArea");
		System.out.println(en.getUserName());
		System.out.println(en.getPassword());
	}

}

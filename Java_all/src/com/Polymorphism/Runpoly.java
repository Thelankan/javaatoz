package com.Polymorphism;

class games{
	
	void play(){
		System.out.println("playing games");
	}
	
}

class football extends games{
	
	void play(){
		System.out.println("playing football");
	}
	
	void rules(){
		System.out.println("hit the goal");
	}
}

class cricket extends games{
	
	void play(){
		System.out.println("playing cricket");
	}
	
	void rules1(){
		System.out.println("hit six");
	}
	
}


public class Runpoly {

	public static void main(String[] args) {
		
//		games g=new games();
//		g.play();
//		
//		football f=new football();
//		f.play();
		
//		games g1=new football();// upcasting 
//		g1.play();
//		
//		football f=(football) g1;//downcasting
//		f.play();
		
		games g=new cricket();
		g.play();
		
//		football f1=(football) new games();// direct down casting is not possible
//		f1.paly();
		

	}

}

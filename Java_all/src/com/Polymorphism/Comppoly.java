package com.Polymorphism;


class a{
	
	void type(int i){
		System.out.println("one arg");
	}
	
	void type(int i,int j){
		System.out.println("two arg");
	}
	
}

public class Comppoly {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		a a1=new a();
		a1.type(10);
		a1.type(10,20);
		
	}

}

package com.Polymorphism;

interface games2{
	
	static final int i=0;
	public abstract void outdoorgames();
	
}

interface games3{
	
	 int i=10;
	public abstract void indoorgames();
	
	
}

class baseball implements games2,games3{
	
	public void outdoorgames(){
		System.out.println("playing baseball");
	}

	@Override
	public void indoorgames() {
		System.out.println("playing badminton");	
	}
}

class newgame extends baseball implements games3 {
	
	public void indoorgames() {
		System.out.println("playing new indoorgame");	
	}
	
	public void outdoorgames(){
		System.out.println("playing new outdoorgame");
	}
}

public class intf {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		baseball b=new baseball();
		b.outdoorgames();
		
		games2 g3=new baseball();
		g3.outdoorgames();
		
	}

}

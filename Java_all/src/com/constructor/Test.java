package com.constructor;

public class Test {

	/*Test()
	{
	
		this(3);
		System.out.println("Default constructor");
		
	}
	
	Test(int x)
	{
		
		this(3,3);
		System.out.println("first constructor");
		System.out.println(x);
	}
	
	Test(int x,int y)
	{
		
		
		System.out.println("Second constructor");
		System.out.println(x*y);
	}*/
	
	Test()
	{
	
		
		System.out.println("Default constructor");
		
	}
	
	Test(int x)
	{
		
		this();
		System.out.println("first constructor");
		System.out.println(x);
	}
	
	Test(int x,int y)
	{
		
		this(5);
		System.out.println("Second constructor");
		System.out.println(x*y);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new Test(3,3);
	}

}


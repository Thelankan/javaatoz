package com.strings;

public class Samplestr3 {

	public static void main(String[] args) {

		
		String str1=new String("Rahul");
		str1.concat("is a dancer");
		
		System.out.println(str1);
		
		
		//String Buffer
		StringBuffer strbuf=new StringBuffer("Amit");
		strbuf.append("is a boss");
		
		System.out.println(strbuf);
		
		//String builder
		StringBuilder strbuilder=new StringBuilder("Mohan");
		strbuilder.append("is a cool guy");
		
		System.out.println(strbuilder);

	}

}

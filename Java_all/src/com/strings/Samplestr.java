package com.strings;

public class Samplestr {

	public static void main(String[] args) {


		String str1="Dhiraj";//literals
		String str2="Dhiraj";
		String str3=new String("Dhiraj");//create object with new key word
		String str4=new String("dhiraj");
		
		System.out.println(str1==str2);
		System.out.println(str1==str3);
		System.out.println(str3==str4);
		System.out.println(str3.equals(str4));
		System.out.println(str3.equals(str1));
		
		System.out.println(str3.equalsIgnoreCase(str4));

	}

}

package execuitable;


//########## Program 1 ###################

/*//compilation error
class A
{
	//method return type should be there
    public Top(String s) 
    {
        System.out.print("A");
    }
}
 
public class execution extends A 
{
    public B(String s) 
    {
        System.out.print("B");
    }
    public static void main(String[] args) 
    {
        new execution("C");
        System.out.println(" ");
    }
}*/

/*//########## Program 2 ###################

//Error: Could not find or load main class execuitable.execution

class Grandparent 
{
    public void Print() 
    {
        System.out.println("Grandparent's Print()"); 
    } 
}
 
class Parent extends Grandparent 
{
    public void Print() 
    {
        System.out.println("Parent's Print()"); 
    } 
}
 
class Child extends Parent 
{
    public void Print()   
    {
        super.Print();
        System.out.println("Child's Print()"); 
    } 
}

public class execution 
{
    public static void main(String[] args) 
    {
        Child c = new Child();
        c.Print(); 

    }
}*/


//########## Program 3 ###################

/*//Error
//The constructor Helper() is not visible
//The field Helper.data is not visible

class Helper
{
    private int data;
    private Helper()
    {
        data = 5;
    }
}
public class execution
{
    public static void main(String[] args)
    {
        Helper help = new Helper();
        System.out.println(help.data);
    }
}*/

//########## Program 4 ###################
/*
//Need to check

public class execution implements Runnable
{
	
    public void run()
    {
        System.out.printf(" Thread's running ");
    }
 
    try{
//        public execution()
//        {
//            Thread.sleep(5000);
//        }
    	
    	System.out.println("hello i am vinay");
    } 
    catch (Exception e) {
        e.printStackTrace();
    }
     
    public static void main(String[] args)
    {
    	execution obj = new execution();
        Thread thread = new Thread(obj);
        thread.start();
        System.out.printf(" GFG ");
    }
    
}*/

/*//########## Program 5 ###################
class Temp
{
    private Temp(int data)
    {
        System.out.printf(" Constructor called ");
    }
    protected static Temp create(int data)
    {
        Temp obj = new Temp(data);
        return obj;
    }
    public void myMethod()
    {
        System.out.printf(" Method called ");
    }
}

public class execution
{
    public static void main(String[] args)
    {
        Temp obj = Temp.create(20);
        obj.myMethod();
    }
}*/

//########## Program 6 ###################

/*public class execution
{
    public Test()
    {
        System.out.printf("1");
        new execution(10);
        System.out.printf("5");
    }
    public Test(int temp)
    {
        System.out.printf("2");
        new execution(10, 20);
        System.out.printf("4");
    }
    public Test(int data, int temp)
    {
        System.out.printf("3");
         
    }
     
    public static void main(String[] args)
    {
    	execution obj = new execution();
         
    }
     
}*/

//########## Program 7 ###################

/*class Base
{
    public static String s = " Super Class ";
    public Base()
    {
        System.out.printf("1");
    }
}
public class execution extends Base
{
    public Derived()
    {
        System.out.printf("2");
        super();
    }
     
    public static void main(String[] args)
    {
        Derived obj = new Derived();
        System.out.printf(s);
    }
}*/
